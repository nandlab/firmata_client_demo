#include <iostream>
#include <firmata/client.hpp>
#include <firmata/serial_port.hpp>
#include <boost/asio/io_context.hpp>

using std::cout;
using std::cerr;
using std::endl;
using boost::asio::io_context;

static void usage(const char *progname) {
    cerr << "Usage: " << progname << " DEVICE" << endl;
}

int main(int argc, char *argv[])
{
    const static unsigned BAUD = 115200;

    if (argc < 2) {
        usage(argv[0]);
        return 1;
    }

    io_context ioc;
    cout << "Start of program" << endl;
    cout << "Connecting to serial..." << endl;
    firmata::serial_port serial(ioc, argv[1]);
    serial.set(firmata::baud_rate(BAUD));
    cout << "Done." << endl;
    cout << "Synchronizing with firmata device..." << endl;
    firmata::client client(serial);
    cout << "Done." << endl;
    cout << "End of program" << endl;
    return 0;
}
